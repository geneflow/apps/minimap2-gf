Minimap2 GeneFlow App
=====================

Version: 2.17-01

This GeneFlow app wraps the minimap2 aligner.

Inputs
------
1. input: Input sequence file in FASTA/FASTQ format.
2. reference: Reference sequence in FASTA format.

Parameters
----------
1. preset: Preset options, including map-ont, map-pb, asm20, sr). Default map-ont.
2. threads: Number of CPU threads. Default 4.
3. output: Output SAM file.
